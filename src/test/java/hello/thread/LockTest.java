package hello.thread;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class LockTest {

  private Integer balance = 0;

  @Test
  public void testReadWriteLock() throws InterruptedException {
    ReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    List<Thread> threads = new ArrayList<>();
    for (int i = 0; i < 10; i++) {
      Thread thread =
          new Thread(
              () -> {
                for (int j = 1; j <= 10; j++) {
                  try {
                    TimeUnit.MILLISECONDS.sleep(100);
                    readWriteLock.writeLock().lock();
                    balance += j;
                  } catch (InterruptedException e) {
                    e.printStackTrace();
                  } finally {
                    readWriteLock.writeLock().unlock();
                  }
                }
              });

      threads.add(thread);
    }

    Thread balancePrinter = new Thread(() -> {
      for (int i = 0; i < 10; i++) {
        try {
          TimeUnit.MILLISECONDS.sleep(100);
          readWriteLock.readLock().lock();
          System.out.println("Current balance: " + balance);
        } catch (InterruptedException e) {
          e.printStackTrace();
        } finally {
          readWriteLock.readLock().unlock();
        }
      }
    });

    for (Thread thread : threads) {
      thread.start();
    }
    balancePrinter.start();

    for (Thread thread : threads) {
      thread.join();
    }
    balancePrinter.join();
    System.out.println("Final balance: " + balance); // expected 550
  }

  @Test
  public void testReentrantLock() throws InterruptedException {
    Lock lock = new ReentrantLock();
    List<Thread> threads = new ArrayList<>();
    for (int i = 0; i < 10; i++) {
      String threadName = "Thread: " + i;
      Thread thread =
          new Thread(
              () -> {
                System.out.println(threadName + " : waiting on lock");
                lock.lock();
                System.out.println(threadName + " : lock acquired");

                try {
                  TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                  e.printStackTrace();
                } finally {
                  System.out.println(threadName + " : releasing lock");
                  lock.unlock();
                }
              });

      threads.add(thread);
    }

    for (Thread thread : threads) {
      thread.start();
    }

    for (Thread thread : threads) {
      thread.join();
    }
  }

}
