package hello.thread;

import org.junit.Test;

import java.time.LocalDateTime;
import java.util.concurrent.*;

public class ExecutorsTest {

  @Test
  public void testSingleThreadedExecutor() throws InterruptedException {
    ExecutorService executorService = Executors.newSingleThreadExecutor();

    executorService.submit(createTask("Task 1"));
    executorService.submit(createTask("Task 2"));
    executorService.submit(createTask("Task 3"));
    executorService.submit(createTask("Task 4"));
    executorService.shutdown();

    TimeUnit.SECONDS.sleep(5);
  }

  @Test
  public void testCachedThreadPool() throws InterruptedException {
    ExecutorService executorService = Executors.newCachedThreadPool();

    executorService.submit(createTask("Task 1"));
    executorService.submit(createTask("Task 2"));
    executorService.submit(createTask("Task 3"));
    executorService.submit(createTask("Task 4"));
    executorService.shutdown();

    TimeUnit.SECONDS.sleep(5);
  }

  @Test
  public void testScheduleExecutorService() throws InterruptedException {
    ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(2);

    scheduledExecutorService.scheduleAtFixedRate(
        createTask("Every 2 second") /* runnable */,
        1 /* initialDelay */,
        2 /* period */,
        TimeUnit.SECONDS /* unit */);

    TimeUnit.SECONDS.sleep(10);
  }


  private Runnable createTask(final String taskName) {
    return new Runnable() {
      @Override
      public void run() {
        System.out.println(taskName + ": started @" + LocalDateTime.now());
        try {
          TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        System.out.println(taskName + ": completed @" + LocalDateTime.now());
      }
    };
  }
}
