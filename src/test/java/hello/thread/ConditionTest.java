package hello.thread;

import org.junit.Test;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ConditionTest {

  @Test
  public void testNotify() throws InterruptedException {
    Lock lock = new ReentrantLock();
    Condition condition = lock.newCondition();
    Thread first = new Thread(() -> waitOnLockObject("First", lock, condition));
    Thread second = new Thread(() -> waitOnLockObject("Second", lock, condition));
    first.start();
    second.start();

    TimeUnit.SECONDS.sleep(5);

    new Thread(
        () -> {
          lock.lock();
          condition.signal();
          lock.unlock();
        })
        .start();

    first.join();
    second.join();
  }

  @Test
  public void testNotifyAll() throws InterruptedException {
    Lock lock = new ReentrantLock();
    Condition condition = lock.newCondition();
    Thread first = new Thread(() -> waitOnLockObject("First", lock, condition));
    Thread second = new Thread(() -> waitOnLockObject("Second", lock, condition));
    first.start();
    second.start();

    TimeUnit.SECONDS.sleep(5);

    new Thread(
        () -> {
          lock.lock();
          condition.signalAll();
          lock.unlock();
        })
        .start();

    first.join();
    second.join();
  }

  private void waitOnLockObject(final String threadName, Lock lock, Condition condition) {
    lock.lock();
    System.out.println(threadName + " thread: waiting on lock");
    try {
      condition.await();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    System.out.println(threadName + " thread: completed");
    lock.unlock();
  }
}
