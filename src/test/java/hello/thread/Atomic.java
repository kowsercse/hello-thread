package hello.thread;

import org.junit.Test;

import java.util.concurrent.atomic.AtomicInteger;

public class Atomic {
  @Test
  public void testAtomicInteger() throws InterruptedException {
    AtomicInteger atomicInteger = new AtomicInteger();

    Thread thread1 =
        new Thread(() -> {
          for (int i = 0; i < 100; i++) {
            atomicInteger.set(i);
            if (atomicInteger.compareAndSet(i, i + 1)) {
              System.out.println("Successfully set to: " + (i + 1));
            }
          }
        });
    Thread thread2 =
        new Thread(() -> {
          for (int i = 0; i < 100; i++) {
            atomicInteger.set(i);
            if (atomicInteger.compareAndSet(i, i + 1)) {
              System.out.println("Failed to set: " + (i + 1));
            }
          }
        });

    thread1.start();
    thread2.start();

    thread1.join();
    thread2.join();
  }
}
