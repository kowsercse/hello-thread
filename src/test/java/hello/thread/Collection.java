package hello.thread;

import org.junit.Test;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;

public class Collection {

  @Test
  public void testCopyOnWriteArrayList() throws InterruptedException {
//    List<String> cars = new ArrayList<>();
    List<String> cars = new CopyOnWriteArrayList<>();
    Thread producerThread = new Thread(
        () -> {
          for (int i = 1; i <= 10; i++) {
            cars.add("Car " + i);
            System.out.println(">> Car" + i);
            try {
              TimeUnit.MILLISECONDS.sleep(200);
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }
        });

    Thread iteratorThread = new Thread(
        () -> {
          System.out.println("Car produced: " + cars.size());
          for (String car : cars) {
            System.out.println("Produced: " + car);
            try {
              TimeUnit.MILLISECONDS.sleep(500);
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }
        });

    producerThread.start();
    TimeUnit.SECONDS.sleep(1); // wait for some cars to be produced
    iteratorThread.start();

    producerThread.join();
    iteratorThread.join();
  }

  @Test
  public void testBlockingQueue() throws InterruptedException {
    BlockingQueue<Integer> blockingQueue = new ArrayBlockingQueue<>(/* size */ 2);
//    BlockingQueue<Integer> blockingQueue = new SynchronousQueue<>();
    Thread quickProducerThread =
        new Thread(
            () -> {
              for (int i = 0; i < 10; i++) {
                try {
                  blockingQueue.put(i);
                  System.out.println(">> " + i);
                } catch (InterruptedException e) {
                  e.printStackTrace();
                }
              }
            });

    Thread slowConsumerThread =
        new Thread(
            () -> {
              for (int i = 0; i < 10; i++) {
                try {
                  Integer popped = blockingQueue.take();
                  System.out.println(popped + " <<");
                  TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                  e.printStackTrace();
                }
              }
            });


    slowConsumerThread.start();
    quickProducerThread.start();

    quickProducerThread.join();
    slowConsumerThread.join();
  }

}
