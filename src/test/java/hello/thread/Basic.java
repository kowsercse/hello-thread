package hello.thread;

import org.junit.Test;

import java.util.concurrent.TimeUnit;

public class Basic {

  private final Object lock = new Object();

  @Test
  public void testRunnableThread() throws InterruptedException {
    Thread runnableThread = new Thread(new Runnable() {
      @Override
      public void run() {
        System.out.println("Running using runnable");
      }
    });
    runnableThread.start();
    runnableThread.join();
  }

  @Test
  public void testLambdaThread() throws InterruptedException {
    Thread lambdaThread =
        new Thread(
            () ->
                System.out.println("Running using lambda"));
    lambdaThread.start();
    lambdaThread.join();
  }

  @Test
  public void testNotify() throws InterruptedException {
    Thread first = new Thread(() -> waitOnLockObject("First"));
    Thread second = new Thread(() -> waitOnLockObject("Second"));
    first.start();
    second.start();

    TimeUnit.SECONDS.sleep(5);

    new Thread(
        () -> {
          synchronized (lock) {
            lock.notify();
          }
        })
        .start();

    first.join();
    second.join();
  }

  @Test
  public void testNotifyAll() throws InterruptedException {
    Thread first = new Thread(() -> waitOnLockObject("First"));
    Thread second = new Thread(() -> waitOnLockObject("Second"));
    first.start();
    second.start();

    TimeUnit.SECONDS.sleep(5);

    new Thread(
        () -> {
          synchronized (lock) {
            lock.notifyAll();
          }
        })
        .start();

    first.join();
    second.join();
  }

  private void waitOnLockObject(final String threadName) {
    synchronized (lock) {
      System.out.println(threadName + " thread: waiting on lock");
      try {
        lock.wait();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      System.out.println(threadName + " thread: completed");
    }
  }

}
