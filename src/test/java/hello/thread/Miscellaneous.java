package hello.thread;

import org.junit.Test;

import java.time.LocalDateTime;
import java.util.concurrent.*;

public class Miscellaneous {

  @Test
  public void testFutureAndCallable() throws InterruptedException, ExecutionException {
    ExecutorService executorService = Executors.newSingleThreadExecutor();
    Future<LocalDateTime> future =
        executorService.submit(
            () -> {
              sleepSilently();
              return LocalDateTime.now();
            }
        );

    while (!future.isDone()) {
      System.out.println(LocalDateTime.now());
      TimeUnit.SECONDS.sleep(1);
    }
    System.out.println("Completed at: " + future.get());
  }

  private void sleepSilently() {
    try {
      TimeUnit.SECONDS.sleep(5);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

}
